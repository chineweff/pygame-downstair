import pygame
import time
import random
display_width = 1280
display_height = 960 
people_startx = display_width * 0.48
people_starty = 100

# R G B
black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
green = (127, 255, 127)

pygame.init()
gameDisplay = pygame.display.set_mode( (display_width, display_height) )
pygame.display.set_caption('QQ')
clock = pygame.time.Clock()

Background = pygame.image.load('background.png')
Background_up = pygame.image.load('up.png')
back = pygame.image.load('back.png')
# people size 67 * 67
peoplew = 67
peopleh = 67
Img = pygame.image.load('XDD.png')

# block size 73 * 12
blockw = 150
blockh = 45
normal_block = pygame.image.load('normal_block.png')
kill_block = pygame.image.load('kill_block.png')
go_block = pygame.image.load('go_block.png')
go_block1 = pygame.image.load('go_block1.png')
go_block2 = pygame.image.load('go_block2.png')
move_block_right1 = pygame.image.load('move_block_right1.png')
move_block_right2 = pygame.image.load('move_block_right2.png')
move_block_left1 = pygame.image.load('move_block_left1.png')
move_block_left2 = pygame.image.load('move_block_left2.png')
# border size 28 * 50
borderw = 28
borderh = 50
border_block = pygame.image.load('border.png')




def people(x, y):
	gameDisplay.blit(Img, (x, y))

def text_objects(text, font, color) :
	textSurface = font.render(text, True, color)
	return textSurface, textSurface.get_rect()
	
def message_display(text, size, x, y, color) :
	largeText = pygame.font.Font('freesansbold.ttf', size)
	TextSurf, TextRect = text_objects(text, largeText, color)
	TextRect.center = (x, y)
	gameDisplay.blit(TextSurf, TextRect)
	#pygame.display.update()

def object(x, y, w, h, color) :
	pygame.draw.rect(gameDisplay, color, [x, y, w, h])
	
def game_over(score):
	message_display('Game Over', 115, display_width/2, display_height/2, red)
	message_display('SCORE: ' + str(score), 115, display_width/2, display_height/2 + 200, green)
	pygame.display.update()
	time.sleep(2)
	game_loop()
	
def game_loop():

	de = False
	de_counter = 0
	x = people_startx
	y = people_starty
	x_change = 0
	y_change = 10
	life = 100
	screen_up = 5
	score = 0
	score_counter = 0
	
	# init block
	oby = 0
	ob = [] # x, y, type, used, move shrink
	for i in range(0, 8):
		
		obx1 = random.randrange(borderw, display_width - blockw - borderw)
		type = random.randrange(0, 5)
		ob.append( [obx1, oby, type, 0, 0])
		oby += 172
		
		
	# init border
	boy = 100
	bo = []
	for _ in range(0, 18):
		bo.append( [0, boy])
		bo.append( [display_width - borderw, boy, 0])
		boy += borderh
	
	
	gameExit = False
	
	while not gameExit:
		
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				quit()

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					x_change = -10
				elif event.key == pygame.K_RIGHT :
					x_change = 10
					
			if event.type == pygame.KEYUP:
				if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
					x_change = 0
		
		x += x_change
		y += y_change
		
		if x > display_width - borderw or x < borderw :
			x -= x_change
		
		if y > display_height :
			game_over(score)
		
		
		# drawing environment
		gameDisplay.blit(Background, [0, 0])
		gameDisplay.blit(Background_up, [0, 100])
		for _, it in enumerate(bo):
			gameDisplay.blit(border_block, [ it[0], it[1] ])
			it[1] -= 3
			if it[1] < 50:
				score_counter += 1
				it[1] = 950
				
		if score_counter > 10:
			score += 1
			score_counter = 0
		gameDisplay.blit(back, [0, 0])
		
		
		# drawing object
		if de == True  :
			de_counter += 1
			if de_counter == 5:
				de = False
				de_counter = 0		
				
		for _, it in enumerate(ob):	
		
			# drawing blocks
			if it[2] == 0:
				gameDisplay.blit(normal_block, [it[0], it[1]] )
			elif it[2] == 1:
				gameDisplay.blit(kill_block, [it[0], it[1]] )
			elif it[2] == 2:
				if y + peopleh > it[1] and it[1] - y < peopleh and it[1] - y > 0 and x + 67 > it[0] and x < it[0] + blockw :
					if de_counter > 0 and de_counter < 4:
						gameDisplay.blit(go_block1, [it[0], it[1]] )
						y -= 8
					else :
						gameDisplay.blit(go_block2, [it[0], it[1]] )
						y -= 8
				else :
					gameDisplay.blit(go_block, [it[0], it[1]] )
			elif it[2] == 3:
				if it[4] == 0 :
					gameDisplay.blit(move_block_right1, [it[0], it[1]] )
					it[4] = 1
				else :
					gameDisplay.blit(move_block_right2, [it[0], it[1]] )
					it[4] = 0
			elif it[2] == 4:
				if it[4] == 0 :
					gameDisplay.blit(move_block_left1, [it[0], it[1]] )
					it[4] = 1
				else :
					gameDisplay.blit(move_block_left2, [it[0], it[1]] )
					it[4] = 0
				
			
			
			# standing on the block
				#not de		  down border in block		  on the block		   on the block		  left border		  right border
			if de == False and y + peopleh > it[1] and it[1] - y < peopleh and it[1] - y > 0 and x + 67 > it[0] and x < it[0] + blockw :
				if it[2] == 1:		# kill
					y = it[1] - 46
					if it[3] == 0:
						life -= 20
						it[3] = 1
				elif it[2] == 2:	# no
					de = True
					if it[3] == 0:
						if life < 100:
							life += 10
						it[3] = 1
				elif it[2] == 3 : # move right
					if it[3] == 0:
						if life < 100:
							life += 10
						it[3] = 1
					y = it[1] - 66
					if x + 4 < display_width - borderw:
						x += 4
				elif it[2] == 4 : # move left
					if it[3] == 0:
						if life < 100:
							life += 10
						it[3] = 1
					y = it[1] - 66
					if x - 4 > borderw:
						x -= 4
				else: 				# normal
					y = it[1] - 66
					if it[3] == 0:
						if life < 100:
							life += 10
						it[3] = 1
			
			
			# hit top border
			if y < 100 :
				life -= 20
				de = True
				y = 110
				
			# dead
			if life <= 0 :
				game_over(score)
						
			it[1] -= 5
			if it[1] < 100 :
				type = random.randrange(0, 5)
				it[0] = random.randrange(borderw, display_width - blockw - borderw)
				it[1] = 1376
				it[2] = type
				it[3] = 0
		
		
		message_display('Floor: ' + str(score), 75, 1050, 50, green) 
		message_display('LIFE: ' + str(life), 60, 150, 50, red)
		people(x, y)
		
		pygame.display.update()
		clock.tick(30)

game_loop()